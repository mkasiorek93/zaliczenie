create database Sklep_odzieżowy;

use Sklep_odzieżowy;

CREATE TABLE Producenci (
    id_producenta INT AUTO_INCREMENT PRIMARY KEY,
    nazwa_producenta VARCHAR(255) NOT NULL,
    adres_producenta VARCHAR(255) NOT NULL,
    nip_producenta VARCHAR(10) NOT NULL UNIQUE,
    data_podpisania_umowy DATE NOT NULL
);

CREATE TABLE Produkty (
    id_produktu INT AUTO_INCREMENT PRIMARY KEY,
    id_producenta INT,
    nazwa_produktu VARCHAR(255) NOT NULL,
    opis_produktu TEXT,
    cena_netto_zakupu DECIMAL(10, 2) NOT NULL,
    cena_brutto_zakupu DECIMAL(10, 2) NOT NULL,
    cena_netto_sprzedazy DECIMAL(10, 2) NOT NULL,
    cena_brutto_sprzedazy DECIMAL(10, 2) NOT NULL,
    procent_VAT_sprzedazy DECIMAL(5, 2) NOT NULL,
    FOREIGN KEY (id_producenta) REFERENCES Producenci(id_producenta)
);

CREATE TABLE Klienci (
    id_klienta INT AUTO_INCREMENT PRIMARY KEY,
    imie VARCHAR(255) NOT NULL,
    nazwisko VARCHAR(255) NOT NULL,
    adres VARCHAR(255) NOT NULL
);

CREATE TABLE Zamowienia (
    id_zamowienia INT AUTO_INCREMENT PRIMARY KEY,
    id_klienta INT,
    id_produktu INT,
    data_zamowienia DATE NOT NULL,
    FOREIGN KEY (id_klienta) REFERENCES Klienci(id_klienta),
    FOREIGN KEY (id_produktu) REFERENCES Produkty(id_produktu)
);

INSERT INTO Producenci (nazwa_producenta, adres_producenta, nip_producenta, data_podpisania_umowy) VALUES
('Nike', 'USA, Beaverton, One Bowerman Drive', '1234567890', '2020-01-01'),
('Adidas', 'Germany, Herzogenaurach, Adi-Dassler-Strasse 1', '2345678901', '2020-02-01'),
('Puma', 'Germany, Herzogenaurach, PUMA Way 1', '3456789012', '2020-03-01'),
('Reebok', 'USA, Boston, 25 Drydock Avenue', '4567890123', '2020-04-01');

INSERT INTO Produkty (id_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_VAT_sprzedazy) VALUES
(1, 'Nike Air Max', 'Sportowe buty do biegania', 300.00, 369.00, 400.00, 492.00, 23),
(1, 'Nike T-shirt', 'Koszulka sportowa', 50.00, 61.50, 70.00, 86.10, 23),
(1, 'Nike Shorts', 'Szorty sportowe', 40.00, 49.20, 55.00, 67.65, 23),
(1, 'Nike Jacket', 'Kurtka sportowa', 150.00, 184.50, 200.00, 246.00, 23),
(2, 'Adidas Ultraboost', 'Buty do biegania', 320.00, 393.60, 420.00, 516.60, 23),
(2, 'Adidas Hoodie', 'Bluza z kapturem', 100.00, 123.00, 130.00, 159.90, 23),
(2, 'Adidas Joggers', 'Spodnie sportowe', 80.00, 98.40, 100.00, 123.00, 23),
(2, 'Adidas Cap', 'Czapka sportowa', 20.00, 24.60, 30.00, 36.90, 23),
(3, 'Puma Sneakers', 'Buty codzienne', 200.00, 246.00, 250.00, 307.50, 23),
(3, 'Puma T-shirt', 'Koszulka codzienna', 45.00, 55.35, 60.00, 73.80, 23),
(3, 'Puma Sweatpants', 'Spodnie dresowe', 70.00, 86.10, 90.00, 110.70, 23),
(3, 'Puma Jacket', 'Kurtka codzienna', 120.00, 147.60, 160.00, 196.80, 23),
(4, 'Reebok Classic', 'Buty codzienne', 150.00, 184.50, 190.00, 233.70, 23),
(4, 'Reebok Tank', 'Koszulka na ramiączkach', 30.00, 36.90, 40.00, 49.20, 23),
(4, 'Reebok Shorts', 'Szorty codzienne', 35.00, 43.05, 45.00, 55.35, 23),
(4, 'Reebok Hoodie', 'Bluza codzienna', 90.00, 110.70, 110.00, 135.30, 23),
(1, 'Nike Sports Bra', 'Biustonosz sportowy', 60.00, 73.80, 80.00, 98.40, 23),
(2, 'Adidas Socks', 'Skarpetki sportowe', 10.00, 12.30, 15.00, 18.45, 23),
(3, 'Puma Bag', 'Torba sportowa', 80.00, 98.40, 100.00, 123.00, 23),
(4, 'Reebok Track Pants', 'Spodnie dresowe', 60.00, 73.80, 75.00, 92.25, 23);

INSERT INTO Klienci (imie, nazwisko, adres) VALUES
('Jan', 'Kowalski', 'Warszawa, ul. Marszałkowska 1'),
('Anna', 'Nowak', 'Kraków, ul. Grodzka 2'),
('Piotr', 'Wiśniewski', 'Łódź, ul. Piotrkowska 3'),
('Maria', 'Wójcik', 'Wrocław, ul. Świdnicka 4'),
('Krzysztof', 'Kamiński', 'Poznań, ul. Półwiejska 5'),
('Katarzyna', 'Lewandowska', 'Gdańsk, ul. Długa 6'),
('Paweł', 'Zieliński', 'Szczecin, ul. Krzywoustego 7'),
('Agnieszka', 'Szymańska', 'Lublin, ul. Krakowskie Przedmieście 8'),
('Tomasz', 'Woźniak', 'Bydgoszcz, ul. Gdańska 9'),
('Magdalena', 'Dudek', 'Katowice, ul. Stawowa 10');

INSERT INTO Zamowienia (id_klienta, id_produktu, data_zamowienia) VALUES
(1, 1, '2024-01-15'),
(2, 2, '2024-01-20'),
(3, 3, '2024-01-22'),
(4, 4, '2024-02-01'),
(5, 5, '2024-02-05'),
(6, 6, '2024-02-10'),
(7, 7, '2024-02-12'),
(8, 8, '2024-02-15'),
(9, 9, '2024-02-20'),
(10, 10, '2024-02-25');
INSERT INTO Zamowienia (id_klienta, id_produktu, data_zamowienia) VALUES
(1, 1, '2024-01-15');

SELECT * FROM Produkty WHERE id_producenta = 1;

SELECT * FROM Produkty WHERE id_producenta = 1 ORDER BY nazwa_produktu;

SELECT AVG(cena_brutto_sprzedazy) AS srednia_cena FROM Produkty WHERE id_producenta = 1;

sELECT 
    nazwa_produktu, 
    cena_brutto_sprzedazy,
    CASE
        WHEN cena_brutto_sprzedazy <= '110.70' THEN 'Tanie'
        ELSE 'Drogie'
    END AS grupa
FROM Produkty
WHERE id_producenta = 1
ORDER BY cena_brutto_sprzedazy;

SELECT DISTINCT nazwa_produktu
FROM Produkty 
JOIN Zamowienia ON Produkty.id_produktu = Zamowienia.id_produktu;

SELECT DISTINCT nazwa_produktu
FROM Produkty 
JOIN Zamowienia ON Produkty.id_produktu = Zamowienia.id_produktu
LIMIT 5;

SELECT SUM(Produkty.cena_brutto_sprzedazy) AS laczna_wartosc_zamowien
FROM Produkty 
JOIN Zamowienia ON Produkty.id_produktu = Zamowienia.id_produktu;

SELECT Zamowienia.*, Produkty.nazwa_produktu
FROM Zamowienia 
JOIN Produkty ON Zamowienia.id_produktu = Produkty.id_produktu
ORDER BY Zamowienia.data_zamowienia;

SELECT * FROM Produkty
WHERE nazwa_produktu IS NULL
   OR opis_produktu IS NULL
   OR cena_netto_zakupu IS NULL
   OR cena_brutto_zakupu IS NULL
   OR cena_netto_sprzedazy IS NULL
   OR cena_brutto_sprzedazy IS NULL
   OR procent_VAT_sprzedazy IS NULL;

SELECT Produkty.nazwa_produktu, Produkty.cena_brutto_sprzedazy, COUNT(*) AS liczba_zamowien
FROM Produkty 
JOIN Zamowienia ON Produkty.id_produktu = Zamowienia.id_produktu
GROUP BY Produkty.id_produktu
ORDER BY liczba_zamowien DESC
LIMIT 1;

SELECT data_zamowienia, COUNT(*) AS liczba_zamowien
FROM Zamowienia
GROUP BY data_zamowienia
ORDER BY liczba_zamowien DESC
LIMIT 1;












